# Scripts

#### [New Sample Repository](new_repo.sh)
##### Usage
`new_repo.sh [directory] [number of users]`
##### Effect
* Creates an empty repository in a named directory 
* Creates a specified number of "users" that track the created repo

#### [Branch Example 0](branch_example_0.sh)
##### Usage
`branch_example_0.sh`
##### Effect
* Generates an example branch history log based on script dictation
* Mostly an example of how to use the Git Shell wrapper in this repository

![Branch Example 0 Screenshot](../images/branch_example_0.png)


#### [Branch Example 1](branch_example_1.sh)
##### Usage
`branch_example_1.sh`
##### Effect
* Provides example Boxcar-based commit structure with Jira ticket tagging

![Branch Example 1 Screenshot](../images/branch_example_1.png)

#### [Query Example](query_example.sh)
##### Usage
`query_example.sh [directory] [item or branches]`

Examples: 

`query_example.sh branch_example_1 bc/1..bc/2`

`query_example.sh .. HEAD~5..HEAD`
##### Effect
* Displays a variety of queryable information
    - Unique list of Jira tickets
    - Unique list of files changed
        + Commented out block to see status over change set (added, modified, etc)
    - List of commits hashes

![Query Example Screenshot](../images/query_example.png)

#### [Git Shell](gitshell.shinc)
##### Purpose
Supporting git function wrappers for use with local repositories created by New Sample Repository.
##### Installation
Source this file to include the function wrappers: `source ${scripts}/gitshell.shinc`

Where `$scripts` is the path to this scripts directory

It's recommended to add your script to this scripts directory, and then add `scripts=$(dirname $(realpath ${0}))` prior to calling the `source` line above, this way the environment can be easily migrated.

Caller must define the following variables:
* `U_`: this is used to determine what base username to use
* `FULL_DIR`: this is used to specify the full directory path to the directory you want the git operations to operate upon

##### Usage
Functions exist to provide a simplified calling interface to run a git operation as a specific user.

Usernames are expected to use some "core" or base name. For a simple example, let's say `U_` is defined as `user`.

The functions in this file expect you to pass an identifier as the first parameter to identify the user you want to operate on. 

Assuming you created, let's say, 2 users, named `${U_}1` and `${U_}2` (or user1 and user2), your script may then call `commit 1 -m "example commit"`.

This will call `git commit -m "example commit"` on user1's subdirectory.

Optionally, if you want to create your own local repository environment mimicking what's created by New Sample Repository and use more "real" names, you can unset `U_`, and just pass the name as the first parameter to each function. `commit myUser -m "example commit"` would run `git commit -m "example commit"` on the directory named by myUser in your defined repository path.

##### Supported Commands
* log (note that three example log graph functions with pretty formatting are also provided, log1, log2, log3)
* commit
* pull
* push
* checkout
* rebase
* merge
* branch
* tag
