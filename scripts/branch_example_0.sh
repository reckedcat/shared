#!/bin/bash

ME=`basename "$0"`
scripts=$(dirname $(realpath ${0}))
DIR="${ME%%.*}"
FULL_DIR=$(realpath $DIR)
U_="user"


help () {
   echo "${ME}: ${1}"
   echo ""
   echo "usage: ${ME}"
   echo ""
}

# Include git bash functions
source ${scripts}/gitshell.shinc


main () {
   checkout 0 -b release
   tag 0 "v0.0.0"
   checkout 0 master
   commit 0 -m "Start Development Here"
   checkout 0 -b "u/${U_}0"
   commit 0 -m "user 0 reporting in [JIRA-0]"
   push 0 -u origin "u/${U_}0"
   push 0 --tags

   echo "pull 1"
   pull 1 --all
   checkout 1 -b "u/${U_}1"
   commit 1 -m "user 1 reporting in [JIRA-1]"
   push 1 -u origin "u/${U_}1"
   push 1

   echo "pull 2"
   pull 2 --all
   checkout 2 -b "u/${U_}2"
   commit 2 -m "user 2 reporting in [JIRA-2]"
   push 2 -u origin "u/${U_}2"
   push 2

   echo "pull all"
   pull 0 -all
   pull 1 -all
   pull 2 -all
}

rm -rf ./$DIR
NUM_USERS=4

$scripts/new_repo.sh $DIR $NUM_USERS

cd $FULL_DIR


printf "%-70s" "Running routine..."
main &>/dev/null
# main 
printf "[ %bDONE%b ]\n" "\e[1;32m" "\e[0m"
echo ""

echo "user2 logs:"
echo "log type 1"
log1 2 --all
echo ""
echo "log type 2"
log2 2 --all
echo ""
echo "log type 3"
log3 2 --all
