#!/bin/bash

ME=`basename "$0"`
DIR=$1
ITEM=$2
# scripts=$(dirname $(realpath ${0}))
# DIR="${ME%%.*}"
# FULL_DIR=$(realpath $DIR)
U_="user"

help() {
   echo "${ME}: ${1}"
   echo ""
   echo "usage: ${ME} [directory] [item or branches]"
   echo ""
   echo "example: ${ME} . HEAD~5..HEAD"
   echo ""
}

# trace output of the passed function, like "set -x" for a single line
# doesn't work for piped commands unless eval is used on string
xtrace() { printf >&2 '%b+ %s%b\n' "\e[1;30m" "$*" "\e[0m"; "$@"; }

main () {

   printf "%b%s%b\n" "\e[0;33m" "Print separated list of Jira tags, excluding merges" "\e[0m"

   tmp="git --git-dir=${DIR}/.git --no-pager log --no-merges --format=\"%b\" ${ITEM} | grep jira | sed 's/\* jira:* //' | sort | uniq"
   xtrace eval $tmp

   printf "\n%b%s%b\n" "\e[0;33m" "Print list of files changed" "\e[0m"
   xtrace git                       \
              --git-dir=$DIR/.git   \
              diff-tree             \
              --no-commit-id        \
              --name-only           \
              -r                    \
              $ITEM

   # Uncomment following block to display list of files with change mode (A: Added, M: Modified, etc)
   # printf "\n%b%s%b\n" "\e[0;33m" "Print list of files changed indicating what changed (status)" "\e[0m"
   # xtrace git                       \
   #            --git-dir=$DIR/.git   \
   #            diff-tree             \
   #            --no-commit-id        \
   #            --name-status         \
   #            -r                    \
   #            $ITEM

   printf "\n%b%s%b\n" "\e[0;33m" "Print list of commit hashes" "\e[0m"
   xtrace git                       \
              --git-dir=$DIR/.git   \
              --no-pager            \
              log                   \
              --no-merges           \
              --format="%h"       \
              ${ITEM}

}


if [[ -z $DIR ]]; then
   help "Empty directory name"
   exit 1
fi

if [[ -z $ITEM ]]; then
   help "Must provide commit or branch diff"
   exit 1
fi

main
