#!/bin/bash

ME=`basename "$0"`
DIR=$1
NUM_USERS=$2

help() {
   echo "${ME}: ${1}"
   echo ""
   echo "usage: ${ME} [directory] [number of users] (optional)[--verbose]"
   echo ""
}

create_repository () {
   mkdir -p $ORIGIN
   cd $ORIGIN
   git --bare init
}

create_first_user () {
   git clone $ORIGIN $PARENT/$U_"0"
   cd $PARENT/$U_"0"
   git config user.name "${U_}0"
   git config user.email "${U_}0@localhost"
   touch .gitignore
   git add .gitignore
   git commit -m "Initial Commit"
   git push
}

create_other_users () {
   for i in $(seq 1 $(expr $NUM_USERS - 1)); do
      git clone $ORIGIN $PARENT/$U_$i
      cd $PARENT/$U_$i
      git config user.name $U_$i
      git config user.email $U_$i@localhost
      git pull
   done
}

main () {
   # Check for empty directory name
   # while test $# -gt 0; do
   #    case "$1" in 
   #       -v|--verbose)
   #          VERBOSE=true
   #          ;;
   #       *)
   #          ;;
   #    esac
   #    shift
   # done

   if [[ -z $DIR ]]; then
      help "Empty directory name"
      exit 1
   fi

   # Test for non-zero/non-empty number of users
   if ! [[ $NUM_USERS =~ [1-9]([0-9])? ]]; then 
      help "Requires non-zero number of users"
      exit 1
   fi



   PARENT=$PWD/$DIR
   ORIGIN=$PARENT/origin.git
   U_="user"

   
   printf "%-70s" "Creating ${NUM_USERS} users in ${DIR}..."

   # if ! [[ -z $VERBOSE ]]; then
      create_repository  &>/dev/null
      create_first_user  &>/dev/null
      create_other_users &>/dev/null
   # else
   #    create_repository
   #    create_first_user
   #    create_other_users
   # fi

   printf "[ %bDONE%b ]\n" "\e[1;32m" "\e[0m"
}

main $1 $2
