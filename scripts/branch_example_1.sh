#!/bin/bash

ME=`basename "$0"`
scripts=$(dirname $(realpath ${0}))
DIR="${ME%%.*}"
FULL_DIR=$(realpath $DIR)
U_="user"


help () {
   echo "${ME}: ${1}"
   echo ""
   echo "usage: ${ME}"
   echo ""
}

# Include git bash functions
source ${scripts}/gitshell.shinc


boxcar1 () {
   checkout 0 -b "bc/1"
   push 0 -u origin bc/1
   checkout 0 -b "bc/1.1"
   push 0 -u origin bc/1.1

   checkout 0 -b "cr/JIRA-0"
   commit 0 -m "user 0 reporting in" -m $'* branch: cr/JIRA-0\n* jira:   [JIRA-0]\n* boxcar: 1\n* build:  1'
   tag 0 "review/1"

   commit 0 -m "another change" -m $'* branch: cr/JIRA-0\n* jira:   [JIRA-0]\n* boxcar: 1\n* build:  1'
   push 0 -u origin cr/JIRA-0
   push 0 --tags

   checkout 0 bc/1.1
   checkout 0 -b "cr/JIRA-1"
   commit 0 -m "more changes" -m $'* branch: cr/JIRA-1\n* jira:   [JIRA-1]\n* boxcar: 1\n* build:  1'
   tag 0 "review/2"
   push 0 -u origin cr/JIRA-1
   push 0 --tags

   checkout 0 bc/1.1
   merge 0 cr/JIRA-1 --commit --no-ff -m "Merge cr/JIRA-1 into bc/1.1" -m $'* branch: bc/1.1\n* jira:   NA\n* boxcar: 1\n* build:  1'
   merge 0 cr/JIRA-0 --commit --no-ff -m "Merge cr/JIRA-0 into bc/1.1" -m $'* branch: bc/1.1\n* jira:   NA\n* boxcar: 1\n* build:  1'
   push 0
   push 0 --tags

   checkout 0 bc/1
   merge 0 bc/1.1 --commit --no-ff -m "Merge bc/1.1 into bc/1" -m $'* branch: bc/1\n* jira:   NA\n* boxcar: 1\n* build:  1'
   push 0
   push 0 --tags

   checkout 0 master
   merge 0 bc/1 --commit --no-ff -m "Merge bc/1 into master" -m $'* branch: master\n* jira:   NA\n* boxcar: 1\n* build:  1'
   push 0
   push 0 --tags
}
boxcar2 () {
   checkout 0 -b "bc/2"
   push 0 -u origin bc/2
   checkout 0 -b "bc/2.1"
   push 0 -u origin bc/2.1

   checkout 0 -b "cr/JIRA-2"
   commit 0 -m "first commit of second boxcar" -m $'* branch: cr/JIRA-2\n* jira:   [JIRA-2]\n* boxcar: 2\n* build:  1'
   tag 0 "review/4"

   commit 0 -m "change 2 electric boogaloo" -m $'* branch: cr/JIRA-2\n* jira:   [JIRA-2]\n* boxcar: 2\n* build:  1'
   push 0 -u origin cr/JIRA-2
   push 0 --tags

   checkout 0 bc/2.1
   checkout 0 -b "cr/JIRA-3"
   commit 0 -m "further updates" -m $'* branch: cr/JIRA-3\n* jira:   [JIRA-3]\n* boxcar: 2\n* build:  1'
   tag 0 "review/3"
   push 0 -u origin cr/JIRA-3
   push 0 --tags

   checkout 0 bc/2.1
   merge 0 cr/JIRA-3 --commit --no-ff -m "Merge cr/JIRA-3 into bc/2.1" -m $'* branch: bc/2.1\n* jira:   NA\n* boxcar: 2\n* build:  1'
   merge 0 cr/JIRA-2 --commit --no-ff -m "Merge cr/JIRA-2 into bc/2.1" -m $'* branch: bc/2.1\n* jira:   NA\n* boxcar: 2\n* build:  1'
   push 0
   push 0 --tags

   checkout 0 bc/2
   merge 0 bc/2.1 --commit --no-ff -m "Merge bc/2.1 into bc/2" -m $'* branch: bc/2\n* jira:   NA\n* boxcar: 2\n* build:  1'
   push 0
   push 0 --tags

   checkout 0 master
   merge 0 bc/2 --commit --no-ff -m "Merge bc/2 into master" -m $'* branch: master\n* jira:   NA\n* boxcar: 2\n* build:  1'
   push 0
   push 0 --tags
}

release () {
   checkout 0 release
   merge 0 master --commit --no-ff -m "Merge master into release" -m $'* branch: release\n* jira:   NA\n* boxcar: 2\n* build:  1'
   tag 0 "v0.2.1"
   checkout 0 master
}

main () {
   checkout 0 -b release
   tag 0 "v0.0.0"
   checkout 0 master
   commit 0 -m "Start Development Here" -m $'* branch: master\n* jira:   NA\n* boxcar: 0\n* build:  0'
   push 0
   push 0 --tags
   boxcar1
   boxcar2
   release

   
}

rm -rf ./$DIR
NUM_USERS=1

$scripts/new_repo.sh $DIR $NUM_USERS

cd $FULL_DIR


printf "%-70s" "Running routine..."
main &>/dev/null
# main 
printf "[ %bDONE%b ]\n" "\e[1;32m" "\e[0m"
echo ""

echo "user0 logs:"
echo "log type 1"
log1 0 --all
echo ""
# echo "log type 2"
# log2 0 --all
# echo ""
# echo "log type 3"
# log3 0 --all
